import cv2
from tkinter import filedialog
from tkinter import *
import win32api
import win32con
from time import sleep
import pyautogui as pg
import matplotlib.pyplot as plt
import keyboard

class Sketch(object):
    def __init__(self):
        self.tk = Tk()
        self.tk.geometry('200x160')
        self.tk['bg'] = 'blue'
        self.l1 = 28
        self.l2 = 80
        bt1 = Button(self.tk, text='Selecionar imagem',
                     command=self.select_img, bg='light blue', width=20)
        bt1.pack()
        self.bt2 = Button(self.tk, text='Mostrar contorno',
                          command=self.show_edge, bg='light blue', width=20)
        self.bt3 = Button(self.tk, text='Desenhar',
                          command=self.init, bg='light blue', width=20)
        self.bt4 = Button(self.tk, text='Calibrar',
                          command=self.calibrar, bg='light blue', width=20)
        self.bt4.pack(side=BOTTOM)
        self.f1 = Frame(self.tk, bg='blue')
        self.entry1 = Entry(self.f1, width=10)
        self.entry2 = Entry(self.f1, width=10)
        self.entry1.pack(side=LEFT)
        self.entry2.pack()
        self.conf = Button(self.tk, text='Confirmar',
                           command=self.fin, bg='light blue', width=20)
        self.tk.mainloop()

    def fin(self):
        self.l1 = int(self.entry1.get())
        self.l2 = int(self.entry2.get())
        self.find_edges()
        self.f1.pack_forget()
        self.conf.pack_forget()

    def calibrar(self):
        self.f1.pack(side=BOTTOM)
        self.conf.pack(side=BOTTOM)

    def init(self):
        self.open_paint()
        self.sweep_img()

    def select_img(self):
        self.patch = filedialog.askopenfilename()
        self.find_edges()
        self.bt2.pack()
        self.bt3.pack()

    def show_edge(self):
        plt.imshow(self.img, cmap='gray')
        plt.show()

    def find_edges(self):
        img = cv2.imread(self.patch, 0)
        smooth_img = cv2.GaussianBlur(img, (7, 7), 0)
        self.img = cv2.Canny(smooth_img, self.l1, self.l2)

    def click(self, x, y):
        win32api.SetCursorPos((x, y))
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0)
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0)

    def open_paint(self):
        pg.press('win')
        sleep(1)
        pg.write('paint')
        sleep(1)
        pg.press('enter')
        sleep(2)
        pg.click(x=307, y=68)
        pg.moveTo(x=129, y=238)

    def sweep_img(self):
        for i in range(self.img.shape[0]):
            for j in range(self.img.shape[1]):
                if self.img[i][j] != 0:
                    self.click(129+j, 238+i)
                    sleep(0.009)
                if keyboard.is_pressed('c'):
                    break

Sketch()
